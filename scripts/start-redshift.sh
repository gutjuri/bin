#!/usr/bin/env bash

redshift -l $(curl -sS ipinfo.io | jq -j .loc | tr ',' ':') & disown

exit 0;
