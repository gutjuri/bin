#!/usr/bin/env bash
###############
### Aliases ###
###############

# cd up multiple directories at a time
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias ......='cd ../../../../../'
alias .......='cd ../../../../../../'
alias ........='cd ../../../../../../../'
alias .........='cd ../../../../../../../../'
alias ..........='cd ../../../../../../../../../'

# Some more ls aliases
alias sl='ls';
ls /dev/shm --hyperlink=auto >/dev/null 2>&1;
if [[ "$?" -eq 0 ]]; then
	alias ll='ls -hAtFl --hyperlink=auto';
	alias la='ls -hA --hyperlink=auto';
	alias l='ls -htFl --hyperlink=auto';
else
	alias ll='ls -hAtFl';
	alias la='ls -hA';
	alias l='ls -htFl';
fi

if ! which tree >/dev/null 2>&1; then
	alias tree='find . | sed -e "s/[^-][^\/]*\//  |/g" -e "s/|\([^ ]\)/|-\1/"';
fi

# less - display line numbers, and allow colour
alias less='less -R'

# Make cp and mv prompt before overwriting
alias cp='cp -i'
alias mv='mv -i'


# Make everything tell us what they're doing
alias mkdir='mkdir -pv'
alias rm='rm -v'
alias rmdir='rmdir -v'


# Make the permissions tweaking commands give us more information about what they are doing
alias chmod='chmod -c'
alias chown='chown -c'
alias chgrp='chgrp -c'

# Quiet, ffmpeg!
alias ffmpeg='ffmpeg -hide_banner'
alias ffprobe='ffprobe -hide_banner'

# Convert flaca files to mp3
flac2mp3() {
	dir="${1}";
	if [[ -z "${dir}" ]]; then dir="."; fi
	find "${dir}" -iname '*.flac' -type f -print0 | nice -n20 xargs -P "$(nproc)" --null --verbose -n1 -I{} sh -c 'old="{}"; new="${old%.*}.mp3"; ffmpeg -i "${old}" -ab 320k -map_metadata 0 -c:v copy -disposition:v:0 attached_pic -id3v2_version 3 "${new}";';
}

mp3cover() {
	cover="${1}";
	dir="${2}";
	
	if [[ -z "${cover}" ]] || [[ -z "${dir}" ]]; then
		echo "Usage:" >&2;
		echo "    mp3cover path/to/cover_image.jpg path/to/album_dir";
		return 0;
	fi
	
	find "${dir}" -type f -iname '*.mp3' -print0 | xargs -0 -P "$(nproc)" eyeD3 --add-image "${cover}:FRONT_COVER:"
}

# Compress PDFs
pdfcompress() {
	local filename="${1}";
	local makebackup="${2}";
	
	if [[ -z "${filename}" ]]; then
		echo "Usage:
	pdfcompress <filename.pdf> [<makebackup>]

...where <filename.pdf> is the pdf to compress, and <makebackup> is any keyword that, if specified, causes a backup of filename.pdf as filename.pdf.bak to be created.";
	fi
	
	if [[ ! -z "${makebackup}" ]]; then cp "${filename}" "${filename}.bak"; fi
	ps2pdf -dPDFSETTINGS=/prepress "${filename}" "${filename}.temp.pdf";
	mv -f "${filename}.temp.pdf" "${filename}";
}

# Make time work properly, but only if the local binary is present
if [[ -f /usr/bin/time ]]; then
	alias time='/usr/bin/time';
fi

# dfh: Show disk usage in a form we can actually understand
alias dfh='df -h'

# lsblkl: more lsblk info
alias lsblkl='lsblk -o NAME,RO,SIZE,RM,TYPE,MOUNTPOINT,LABEL,VENDOR,MODEL'


# zcat: gunzip & display commpressed gz files.
which zcat >/dev/null 2>&1;
if [[ "$?" -ne 0 ]]; then
	alias zcat='gunzip -c'
fi

# Alias to clear the screen for real
alias cls='printf "\033c"'

# mkfifo -> mkpipe
alias mkpipe='mkfifo'

# OpenRC helpers
if command -v rc-service >/dev/null 2>&1; then
	alias service='rc-service';
fi
if command -v rc-update >/dev/null 2>&1; then
	alias update='rc-update';
fi
if command -v rc-status >/dev/null 2>&1; then
	alias status='rc-status';
fi

# Get scrollback back - ref https://github.com/mobile-shell/mosh/issues/122#issuecomment-623792008
alias mosh='mosh --no-init';

# allow us to create a directory and immediately move into it
mcd() { mkdir -pv "$1" && cd "$1";}

# Set the window title
set-title() {
	echo -e '\033]2;'$1'\033\\';
	# Tell set_bash_prompt that we don't want it to reset the terminal title again
	TITLE_SET_MANUALLY=true;
}

# Display a sorted list of processes and their swap usage 
# Source: https://www.cyberciti.biz/faq/linux-which-process-is-using-swap/
swaptop() {
	for file in /proc/*/status ; do awk '/VmSwap|Name/{printf $2 " " $3}END{ print ""}' $file; done | sort -k 2 -n -r | less
}

# Text Processing
# From https://www.reddit.com/r/tinycode/comments/c63gr6/deblank_remove_blank_lines_from_a_file_or_stdin/
alias deblank='grep -vE "^\s*$"'

# hr
alias hrh='hr - - = - -'

# termbins
if which ncat >/dev/null 2>&1; then
	alias sbrlbin='nc starbeamrainbowlabs.com 9999';
	alias termbin='nc termbin.com 9999';
elif which netcat >/dev/null 2>&1; then
	alias sbrlbin='netcat starbeamrainbowlabs.com 9999';
	alias termbin='netcat termbin.com 9999';
elif which nc >/dev/null 2>&1; then
	alias sbrlbin='nc starbeamrainbowlabs.com 9999';
	alias termbin='nc termbin.com 9999';
else
	alias sbrlbin='echo "Failed to find ncat, netcat, or nc :-("';
	alias termbin='echo "Failed to find ncat, netcat, or nc :-("';
fi

# termbin for any kind of file
transfer() { if [ $# -eq 0 ]; then echo "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi 
tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; command rm -f $tmpfile; }

# gitignore.io interface
gitignore() { curl -L -s "https://www.gitignore.io/api/$*"; }

# git v2.15+
#if [[ "$(git --version | awk 'BEGIN { FS="." } { print $2 }')" -ge 15 ]]; #then
#	alias 'git-diff'='git diff --color-moved'
#	alias 'git-show'='git show --color-moved'
#fi

# Get / Set clipboard contents
alias setclip='xclip -selection c'
alias getclip='xclip -selection clipboard -o'

# Ubuntu's inbuilt image previewer. I'll never be able to remember eog...
alias image-preview=eog

# URL Encode / Decode
# From https://unix.stackexchange.com/a/216318/64687
alias urlencode='python -c "import urllib, sys; print urllib.quote(sys.argv[1] if len(sys.argv) > 1 else sys.stdin.read()[0:-1], \"\")"'
alias urldecode='python -c "import urllib, sys; print urllib.unquote(sys.argv[1] if len(sys.argv) > 1 else sys.stdin.read()[0:-1])"'

# Activate coloured output in ncdu
alias ncdu='ncdu --color dark'

# Single-level recursive git
rgit() {
	# TODO: Make this work in parallel
	for dirname in $(find . -maxdepth 2 -type d -name .git); do
		pushd $(dirname $dirname) >/dev/null;
		echo "Repository: ${PWD}"
		git $@;
		popd >/dev/null;
	done
}
rgit-update() {
	# BUG: pushd needs to be quiet, and git log is still opening less
	for dirname in $(find . -maxdepth 2 -type d -name .git); do
		pushd $(dirname $dirname);
		pwd
		git fetch;
		
		git log --oneline --graph master..origin/master | cat;
		[[ "$(git log --oneline --graph master..origin/master | wc -l)" -gt 0 ]] && read -p "Merge Changes? (Enter) - (^C to abort)";
		
		git merge;
		
		popd;
	done
}

# colourise man
_colorman() {
  env \
    LESS_TERMCAP_mb=$(printf "\e[1;35m") \
    LESS_TERMCAP_md=$(printf "\e[1;34m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[7;40m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;33m") \
      "$@"
}
man() { _colorman man "$@"; }

# Quickly jump around places
telepeek() {
	find . -type d 2>/dev/null | grep -iP "$@" | cat -n | sed 's/^[ 0-9]*[0-9]/\o033[34m&\o033[0m/' | less -R
}
teleport() {
	cd "$(find . -type d 2>/dev/null | grep -m1 -iP "$@")";
}
telepick() {
	telepeek $@;
	read -p "jump to index: " line_number;
	cd "$(find . -type d 2>/dev/null | grep -iP "$1" | sed "${line_number}q;d")";
}

# Archivebox remote
# TODO: Make this work remotely by going via starbeamrainbowlabs.com if needed
__archive-url() {
	url="$1";
	if [[ "${url}" == "" ]]; then read -p "Url to archive: " url; fi
	ssh elessar -t "cd /srv/ArchiveBox; ./archive-url \"${url}\"";
}
alias archive-url=__archive-url;

# Download a Youtube video, convert to audio, and grab the thumbnail
alias ytat="youtube-dl --output '%(title)s.%(ext)s' -x --audio-format mp3 --write-thumbnail";

# Quick slurm aliases
alias squeueu='squeue -u "${USER}"';
alias squeuea='squeue --all --long';

# Connect to the University Palo Alto VPN
# CAUTION: If you use this, it's at your own risk
univpn() {
	set-title openconnect
	sudo openconnect --protocol gp pa-vpn.hull.ac.uk --usergroup gateway --csd-wrapper "${HOME}/bin/scripts/hipreport.sh" --local-hostname="$(dd if=/dev/urandom bs=16 count=1 2>/dev/null | od -An -vt x8 | tr -d ' \n')" --os=linux-64;
}
