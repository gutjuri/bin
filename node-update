#!/usr/bin/env bash
set -e;

# Make sure the current directory is the location of this script to simplify matters
cd "$(dirname $(readlink -f $0))";

source "./lantern-build-engine/lantern.sh";

task_begin "Removing old versions";
find "/usr/local/lib" -maxdepth 1 -iname "node-*" | sort | sudo xargs --verbose rm -rf;

task_begin "Getting latest version number"
VERSION=v${1:-$(curl https://nodejs.org/dist/index.json | sed -e 's/^.*"version":"\([^"]*\)".*$/\1/' | head -n 2 | tail -n -1 | cut -c 2-)}
arch=$(uname -m);
if [ "${arch}" = "x86_64" ]; then
	arch="x64";
fi
NODEJS=node-${VERSION}-linux-${arch}

task_begin "Downloading $VERSION of node.js"
curl -s https://nodejs.org/dist/${VERSION}/${NODEJS}.tar.xz | tar xvfJ -

task_begin "Setting ownership of /usr/local to root"
sudo chown -R root:root /usr/local

task_begin "Moving extracted node.js binaries"
if [ "$(pwd)" != "/usr/local/lib" ]; then
	sudo rm -rf /usr/local/lib/${NODEJS} && sudo mv ${NODEJS} /usr/local/lib
fi

task_begin "Creating symlinks"
sudo rm -f /usr/local/bin/{iojs,node,npm,node-gyp}
sudo ln -s /usr/local/lib/${NODEJS}/bin/node /usr/local/bin/node
sudo ln -s /usr/local/lib/${NODEJS}/bin/npm /usr/local/bin/npm

task_begin "Setting global prefix"
npm config set prefix /usr/local
sudo npm config set prefix /usr/local

echo "Node.js version: $(node -v), npm version: $(npm -v)"


###############################################################################

# Update npm - requires forcing through 'cause it claims it didn't create the initial npm symlink
sudo npm install --global --force npm;

which npm-check >/dev/null 2>&1;
if [[ "$?" -eq 0 ]]; then
	sudo npm-check -gu;
else
	echo "npm-check not detected. Install it to update global npm packages automatically.";
fi
