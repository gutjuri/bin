#!/usr/bin/env bash

echo -ne "Initialsing cloudsurfer: ";

### Settings ###

aliases_url="https://gitlab.com/sbrl/bin/-/raw/master/.bash_aliases";

################
echo -ne "s";


### Aliases file ###

aliases_tmp_file=$(mktemp --suffix ".cloudjumper.sh");

curl -o "${aliases_tmp_file}" "${aliases_url}" 2>/dev/null;
ec_alias_download="$?";
if [[ "${ec_alias_download}" -ne "0" ]]; then
	echo "E";
	echo "Error\! Got exit code ${ec_alias_download} from curl when downloading the bash_aliases file";
	exit ${ec_alias_download};
fi;
chmod u+x "${aliases_tmp_file}";

source "${aliases_tmp_file}";

####################
echo -ne "+a";

### Prompt ###

# Set the prompt to a cyan / blue colour to let everyone know we've enabled cloudjumper successfully
PS1="\[\033[1m\]\[\033[34m\]${PS1}\[\033[0m\]";

##############
echo -ne "+p";

### Cleanup ###

rm "${aliases_tmp_file}" >/dev/null;

###############
echo -ne "+c";

echo " - done";
