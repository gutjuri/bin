# bin
My Linux `~/bin` folder, including my `.bashrc` and `.bash_aliases`.

This repository will contain all the various tools and / or commands I find that I put in my `~/bin` folder.

Here's a list of the most interesting ones, along with what they do and where they came from (assuming I can find / remember):

 - [style](https://github.com/LuRsT/style) - Add style to your output
 - [hr](https://github.com/LuRsT/hr) - A horizontal ruler for your terminal
 - [dropbox_uploader.sh](https://github.com/andreafabrizi/Dropbox-Uploader) - Dropbox Uploader is a BASH script which can be used to upload, download, list or delete files from Dropbox.
 - swapusage - Shows swap usage by process. Useful for determining which services to restart if you aren't allowed to fiddle with your swap.
 - extract - Extracts several different types of archive.
 - mail-service-restart - Restarts all service related to my mail server. Currently needs upgrading to use systemd.
 - decrypt - Decrypts something encrypted with a password with openssl.
 - encrypt - Encrpyts something with a password with openssl.
 - splitdotmbox.sh - Splits a `.mbox` file into it's component messages
 - ctx - Shows contact switches by process in real time
 - ps_mem - Shows memory usage by process.
 - node-update - Adapted from a bash script given in a github issue for io.js, this script will update you to the latest version of node. I think that there's a bug with it, but it works well enough for me.
 - stealtime - Tells you how much of your CPU time has been stolen by other virtual machines.
 - [speedtest-cli](https://github.com/sivel/speedtest-cli)
 - mountunishares - A script that automatically mounts my university file shares.
 - pd - A directory stack helper. Insanely useful - I think I picked it up from a superuser answer somewhere.
 - [transfer](https://transfer.sh/) - A look that looks very useful. Like termbin.com, but for any kind of file. Keeps files for 14 days, and has an uppder limit of 5GB.
 - [xidel](http://www.videlibri.de/xidel.html) - A (what looks like) a brilliant html extraction tool. Supports various selector languages.
 - [micro](https://github.com/zyedidia/micro) - A new (experimental) terminal-based text editor.
 - [tig](http://jonas.nitro.dk/tig/) - A terminal-based git repository browser.
 - backup - A script I wrote to make using Duplicity easier.
 - genimagecomparisonframes - Another script I wrote to generate the frames for the first half of [this video](https://starbeamrainbowlabs.com/blog/images/20160525-Tessellator-Comparison.webm).
 - [dtach](http://dtach.sourceforge.net/) - A minimal alternative to `screen` and `tmux`.
 - [commacd](https://github.com/shyiko/commacd) - A faster way to move around.
 - drop_pagecache - A quick script I wrote to drop the linux page cache.
 - [hastebin](https://gist.github.com/flores/3670953) - A [hastebin](http://hastebin.com/) CLI.
 - [quickshare](https://github.com/netham91/Quick-Share) - A quick way to share files via HTTP.
 - [cscz](https://git.starbeamrainbowlabs.com/sbrl/cscz) - The one and only C# class generator. A little timesaving tool I built myself, written in C# or course :D
 - [repren](https://github.com/jlevy/repren) - A good bulk file renaming tool.
 - [goldilocks](http://unix.stackexchange.com/a/124460/64687) - A script that displays all of the possible terminal colours.
 - [catimg](https://github.com/posva/catimg) - A cool script to display images in the terminal. Useful when you're sshing into servers.
 - [tldr](http://github.com/pepa65/tldr-bash-client) - A bash client for 
[tldr-pages](https://github.com/tldr-pages/tldr).
 - [git ignore](https://gitignore.io/) - A really useful subcommand for git that allows you to generate ignore files automagically.
 - organise-photos - A small bash script I wrote that organises the photos in a directory by year & month.
 - [ogg-cover-art](https://github.com/acabal/scripts/blob/master/ogg-cover-art) - A script that adds cover art to ogg files.
 - senddir - A small script I wrote to send whole directories over the network as a temporary thing. Uses netcat + gzip + tar.
 - recvdir - The counterpart to the above that receives the directory sent.
 - brightness.sh - Another small script I've written that controls the brightness of my screen. It works for me, but it may not work for you.
 - ptr - A small script I wrote that uses `dig` to do PTR (reverse DNS) lookups, since I have to keep looking the command up every time :P
 - * clip
	 - setclip - Sets the clipboard to the contents of the standard input
	 - getclip - Sends the current clipboard contents out of the standard output
	 - listenclip - Listens on localhost:5556 and pipes any data recieved into the clipboard. Make sure you're firewall is working correctly!
	 - sendclip - Pipes stdin to localhost:5556. Useful in conjuntion with ssh port forwarding and the tool above.
	 - serveclip - Serves the clipboard on port 5557. Doesn't close the connection automatically, sadly - pathes welcome!
 - [shellshare](https://shellshare.net/) - A live terminal broadcasting utility.
 - [colortrans](https://gist.github.com/MicahElliott/719710) - Convert rgb colours to xterm ones
 - nautilus-here - A quick command that  opens nautilus in the current directory. For lazy people :P
 - ssh-retry - I forget where it came from, but this command reepeatedly calls ssh until it succeeds in connecting.
 - quote-get - Displays a random quote from a remote file. The quote stays the same until the following day. The remote file is cached locally for convenience :smiley_cat:
 - teleport / telepeek / telepick - A pair of quick functions in my `.bash_aliases` file for quickly jumping about. Works best if you've got a decent amount of free RAM for the cache.
 - [reflac](https://github.com/chungy/reflac) - Small flac recompression script
 - chronicle - My take on another script with the same name on GitHub. Basically a journaling helper script.

## cloudsurfer.sh
`cloudsurfer.sh` is a new script I'm writing to bring some of the customisations I store here in this repository to terminals on shared machines - where I can't modify `.bashrc`, for example.

It's designed to be executed in a single bash context, and leave no trace on the host machine after the customisations are applied - just in case the bash process crashes! If need be, it can also be executed in multiple separate bash processes and will bring the customisations to each one separately - without interfering with the rest of the machine.

Due to it's transient nature, cloudsurfer is bound to be more lightweight and be missing a few features of the full list that my main bin folder can provide, but the aim is to make a foreign terminal feel a bit more like home :smiley_cat:

Here's a list of things it currently supports:

 - Aliases - by dynamically loading the main `.bash_aliases`
 - Colouring the prompt to remind you that cloudsurfer is active
 - _(Got an idea of something to include? File an issue, or open a pull request!)_

To use it, simply paste this into the target terminal:

```bash
   cstemp=$(mktemp); curl -o ${cstemp} -L https://tinyurl.com/sbrlcs; chmod +x ${cstemp}; source ${cstemp}; rm ${cstemp}; cstemp=;
```

and then to drop out of cloudsurfer again:

```bash
exec bash && reset
```

## Disclaimer
I don't own many of the tools in this repository. If you are an owner of one of these tools and I haven't linked to you, please let me know as I probably forgot.
